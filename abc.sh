#! /bin/bash


# Setting IFS (input field separator) value as ","
IFS=','

# Reading the split string into array
read -ra arr <<< "$1"

# Print each value of the array by using the loop
for val in "${arr[@]}";
do
x=`expr $val % 400`
  
y=`expr $val % 100`

z=`expr $val % 4`

if [ `expr $x` -eq 0 ]  || [ `expr $y` -ne 0 ] && [ `expr $z` -eq 0 ]
then
echo "Entered year - $val is a leap year"
else
echo "Entered year - $val is not a leap year "
fi
done
