#!/bin/sh

for a in "$@"
do 

x=`expr $a % 400`

y=`expr $a % 100`

z=`expr $a % 4`

if [ `expr $x` -eq 0 ]  || [ `expr $y` -ne 0 ] && [ `expr $z` -eq 0 ]
then
echo "Entered year - $a is a leap year"
else
echo "Entered year - $a is not a leap year "
fi
done

