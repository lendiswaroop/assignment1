

if [ -z "$1" ]
then
      echo "mathematic operation required"
      exit 1
else
      echo "$2"
fi

if [ -z "$2" ]
then
      echo "value one for calculation is empty exiting operation"
      exit 1
else
      echo "first value for calculation is $1 "
fi


if [ -z "$3" ]
then
      echo "value two for calculation is empty exiting operation"
      exit 1
else
      echo "second value for calculation is $3"
fi



case $2 in
  +)res=`echo $1 + $3 | bc`
  ;;
  -)res=`echo $1 - $3 | bc`
  ;;
  *)res=`echo $1 * $3 | bc`
  ;;
  /)res=`echo $1 / $3 | bc`
  ;;
  %)res=`echo $1 % $3 | bc`
  ;;
esac
echo "Result : $res"

